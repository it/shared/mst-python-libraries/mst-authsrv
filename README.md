# mst-authsrv

[[_TOC_]]

## Installation

Command Line:

```bash
pip install mst-authsrv --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-authsrv
```

## Usage

mst-authsrv provides an interface to authsrv for credential retrieval.

```python
from mst.authsrv import AuthSRV

creds = AuthSRV().fetch("ads")
creds_listmgr = AuthSRV().fetch("ads", "listmgr", "listmgr")
```
